//
//  StartViewController.swift
//  ARMemoryTester
//
//  Created by Samuel Norling on 2019-02-17.
//  Copyright © 2019 Samuel Norling. All rights reserved.
//

import Foundation
import UIKit

class StartViewController: UIViewController {
    
    @IBAction func showAR(_ sender: Any) {
        DispatchQueue.main.async {
            let arViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "arView") as! ViewController
            self.dismiss(animated: true, completion: nil)
            self.present(arViewController, animated: false, completion: nil)
        }
    }
}
